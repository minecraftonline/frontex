/*
 * Copyright (C) 2017 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.frontex;

import com.minecraftonline.frontex.dao.Note;
import com.minecraftonline.frontex.dao.Player;
import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.spongepowered.api.profile.GameProfile;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.ban.*;

/**
 *
 * @author Willem Mulder
 */
public class BanService implements org.spongepowered.api.service.ban.BanService {

    @Override
    public Collection<Ban.Profile> getBans() {
        return Note.findByType(Note.Type.BAN, stream ->
                stream.map(BanService::toBan)
                    .collect(Collectors.toSet()));
    }

    @Override
    public Collection<Ban.Profile> getProfileBans() {
        return this.getBans();
    }

    @Override
    public Collection<Ban.Ip> getIpBans() {
        return Collections.emptySet();
    }

    @Override
    public Optional<Ban.Profile> getBanFor(GameProfile profile) {
        Optional<Player> player = Player.findByUUID(profile.getUniqueId());
        if (!player.isPresent()) {
            return Optional.empty();
        }

        return Note.findByPlayer(player.get(),
                stream -> stream.filter(Note::isBan)
                    .sorted(Comparator.reverseOrder())
                    .map(BanService::toBan)
                    .findFirst());
    }

    @Override
    public Optional<Ban.Ip> getBanFor(InetAddress address) {
        return Optional.empty();
    }

    @Override
    public boolean isBanned(GameProfile profile) {
        Optional<Player> player = Player.findByUUID(profile.getUniqueId());
        if (!player.isPresent()) {
            return false;
        }

        return Note.findByPlayer(player.get(), stream -> stream.anyMatch(Note::isBan));
    }

    @Override
    public boolean isBanned(InetAddress address) {
        return false;
    }

    @Override
    public boolean pardon(GameProfile profile) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean pardon(InetAddress address) {
        return false;
    }

    @Override
    public boolean removeBan(Ban ban) {
        return withBanProfile(ban, this::pardon, false);
    }

    @Override
    public Optional<? extends Ban> addBan(Ban ban) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasBan(Ban ban) {
        return withBanProfile(ban, this::isBanned, false);
    }

    /**
     * Convert a Frontex (ban) note into a {@link Ban.Profile}.
     * @param note The {@link Note} to convert
     * @return A {@link Ban.Profile} containing the source as {@link Text}
     */
    public static Ban.Profile toBan(Note note) {
        Player player = note.getPlayer();
        return (Ban.Profile) Ban.builder()
                .type(BanTypes.PROFILE)
                .profile(GameProfile.of(player.getUuid(), player.getName()))
                .reason(Text.of(note.getMessage()))
                .startDate(note.getDatetime())
                .source(Text.of(note.getAuthor().getName()))
                .build();
    }

    private static <R> R withBanProfile(Ban ban, Function<GameProfile, R> func, R ipResult) {
        return ban.getType() == BanTypes.PROFILE
                ? func.apply(((Ban.Profile) ban).getProfile())
                : ipResult;
    }
}
