/*
 * Copyright (C) 2017 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.frontex.dao;

import be.bendem.sqlstreams.Query;
import be.bendem.sqlstreams.Update;
import com.minecraftonline.frontex.Frontex;
import com.minecraftonline.frontex.sql.UpdateReturning;
import lombok.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * DAO for notes.
 * This class' natural ordering is based on the database ID.
 *
 * @author Willem Mulder
 */
@Getter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Note implements Comparable<Note> {
    /**
     * The database ID of this note.
     * A value of -1 means that this note is not yet in the database.
     */
    int id = -1;

    /**
     * The {@link Instant} this note was made.
     */
    Instant datetime;

    /**
     * The player to whom this note belongs.
     */
    Player player;

    /**
     * The player who made this note.
     */
    Player author;

    /**
     * The type of the note.
     */
    @Setter
    Type type;

    /**
     * The note message.
     */
    String message;

    /**
     * Creates a new note with the given parameters.
     * @param datetime  The {@link Instant} at which the note was issued
     * @param player    The {@link Player} to whom the note was issued
     * @param author    The {@link Player} who issued the note
     * @param type      The {@link Type} of the issued note
     * @param message   The contents of the issued note
     */
    public Note(@NonNull Instant datetime, @NonNull Player player, @NonNull Player author, @NonNull Type type, @NonNull String message) {
        this.datetime = datetime;
        this.player = player;
        this.author = author;
        this.type = type;
        this.message = message;
    }

    /**
     * Saves local changes to the database.
     * @return {@code true} if the associated database record was updated or inserted, {@code false} otherwise.
     */
    public boolean save() {
        if (id == -1) {
            // New record, insert
            try (UpdateReturning update = new UpdateReturning(Frontex.getInstance().getSql(),
                            "INSERT INTO notes (datetime, player_id, author_id, type, message) VALUES (?, ?, ?, ?, ?)")
                    .with(datetime, player, author, type, message)) {
                update.execute();
                Optional<Integer> newId = update.generated(rs -> rs.getInt(1)).findFirst();

                if (newId.isPresent()) {
                    id = newId.get();
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            try (Update update = Frontex.getInstance().getSql()
                    .update("UPDATE notes SET datetime = ?, player_id = ?, author_id = ?, type = ?, message = ? WHERE id = ?")
                    .with(datetime, player, author, type, message, id)) {
                return update.count() > 0;
            }
        }
    }

    /**
     * Removes this note from the database.
     * @return {@code true} if the note was removed from or never saved to the database, {@code false} otherwise
     */
    public boolean delete() {
        if (id == -1) return true;

        try (Update update = Frontex.getInstance().getSql()
                .update("DELETE FROM notes WHERE id = ?")
                .with(id)) {
            return update.count() > 0;
        }
    }

    /**
     * Returns whether this note is a ban note.
     * @return {@code true} if and only if {@code getType() == Note.Type.BAN}
     */
    public boolean isBan() {
        return this.getType() == Note.Type.BAN;
    }

    @Override
    public int compareTo(Note other) {
        return this.id - other.id;
    }

    /**
     * Queries all notes from the database, and applies the given function to them.
     * @param function  The function to apply to the notes
     * @param <T>       The return type of {@code function}
     * @return          The value returned by applying {@code function} to the notes
     */
    public static <T> T all(Function<Stream<Note>, T> function) {
        try (Query q = Frontex.getInstance().getSql().query("SELECT * FROM notes")) {
            return function.apply(q.map(Note::fromResultSet));
        }
    }

    /**
     * Queries all notes for the given player from the database, and applies the given function to them.
     * @param player    The player for whom to retrieve the notes
     * @param function  The function to apply to the notes
     * @param <T>       The return type of {@code function}
     * @return          The value returned by applying {@code function} to the notes
     */
    public static <T> T findByPlayer(@NonNull Player player, Function<Stream<Note>, T> function) {
        try (Query q = Frontex.getInstance().getSql().query("SELECT * FROM notes WHERE player_id = ?").with(player)) {
            return function.apply(q.map(Note::fromResultSet));
        }
    }

    /**
     * Queries all notes made by the given player from the database, and applies the given function to them.
     * @param author    The author of whom to retrieve the notes
     * @param function  The function to apply to the notes
     * @param <T>       The return type of {@code function}
     * @return          The value returned by applying {@code function} to the notes
     */
    public static <T> T findByAuthor(@NonNull Player author, Function<Stream<Note>, T> function) {
        try (Query q = Frontex.getInstance().getSql().query("SELECT * FROM notes WHERE author_id = ?").with(author)) {
            return function.apply(q.map(Note::fromResultSet));
        }
    }

    /**
     * Queries all notes with the given type from the database, and applies the given function to them.
     * @param type      The type of the notes to retrieve
     * @param function  The function to apply to the notes
     * @param <T>       The return type of {@code function}
     * @return          The value returned by applying {@code function} to the notes
     */
    public static <T> T findByType(@NonNull Type type, Function<Stream<Note>, T> function) {
        try (Query q = Frontex.getInstance().getSql().query("SELECT * FROM notes WHERE type = ?").with(type)) {
            return function.apply(q.map(Note::fromResultSet));
        }
    }

    /**
     * Queries all notes with the given type from the database, and returns the number of players who have that note type.
     * @param type      The type of the notes to retrieve
     * @return          The count of distinct notes of this type as applied to players
     */
    public static long countDistinctByType(@NonNull Type type) {
        try (Query q = Frontex.getInstance().getSql().query("SELECT COUNT(DISTINCT player_id) FROM notes WHERE type = ?").with(type)) {
            return q.map(rs -> rs.getLong(1)).findFirst().orElse((long) 0);
        }
    }

    protected static Note fromResultSet(ResultSet rs) throws SQLException {
        return new Note(
                rs.getInt("id"),
                rs.getTimestamp("datetime").toInstant(),
                Player.find(rs.getInt("player_id")).get(),
                Player.find(rs.getInt("author_id")).get(),
                Type.find(rs.getInt("type")).get(),
                rs.getString("message")
        );
    }

    /**
     * Enum representing the different types of note
     */
    @RequiredArgsConstructor
    public enum Type {
        LOG(0),
        INFO(1),
        WARN(2),
        BAN(3);

        @Getter
        private final int id;

        /**
         * Retrieve a {@link Type} by its id.
         * @param id    The id of the {@link Type} to retrieve
         * @return      An {@link Optional} containing the {@link Type} if found, {@link Optional#empty()} otherwise.
         */
        public static Optional<Type> find(int id) {
            Type[] values = values();
            return id >= 0 && id < values.length ? Optional.of(values[id]) : Optional.empty();
        }
    }


    static {
        Frontex.getInstance().getSql()
                .registerCustomBinding(Instant.class, (stmt, idx, instant) -> stmt.setTimestamp(idx, Timestamp.from(instant)))
                .registerCustomBinding(Note.class, (stmt, idx, note) -> stmt.setInt(idx, note.getId()))
                .registerCustomBinding(Type.class, (stmt, idx, type) -> stmt.setInt(idx, type.getId()));
    }
}
