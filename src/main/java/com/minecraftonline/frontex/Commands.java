package com.minecraftonline.frontex;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Streams;
import com.minecraftonline.frontex.dao.Note;
import com.minecraftonline.frontex.dao.Player;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.SpongeEventFactory;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.user.BanUserEvent;
import org.spongepowered.api.event.user.PardonUserEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.channel.type.PermissionMessageChannel;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Tuple;
import org.spongepowered.api.util.ban.Ban;
import org.spongepowered.api.util.ban.BanTypes;

public final class Commands {

    public static final Map<Note.Type, Tuple<TextColor, TextColor>> NOTE_TYPE_COLOURS = ImmutableMap.of(
            Note.Type.LOG,  Tuple.of(TextColors.DARK_GRAY, TextColors.GRAY),
            Note.Type.INFO, Tuple.of(TextColors.BLUE, TextColors.AQUA),
            Note.Type.WARN, Tuple.of(TextColors.DARK_RED, TextColors.RED),
            Note.Type.BAN,  Tuple.of(TextColors.WHITE, TextColors.DARK_RED)
    );

    public static final DateTimeFormatter DATETIME_FORMAT =
            DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss O").withZone(ZoneId.systemDefault());

    public static final Supplier<CommandException> PLAYER_NOT_FOUND =
            () -> new CommandException(Text.of(TextColors.RED, "Player not found"));
    public static final Supplier<IllegalStateException> PLAYER_NOT_IN_DATABASE =
            () -> new IllegalStateException("Player not found in database, please report this");
    public static final Supplier<IllegalStateException> AUTHOR_NOT_IN_DATABASE =
            () -> new IllegalStateException("You weren't found in the database, please report this");

    public static final MessageChannel STAFF_CHANNEL = new PermissionMessageChannel("frontex.notify") {
        @Override
        public Optional<Text> transformMessage(Object sender, MessageReceiver recipient, Text original, ChatType type) {
            return Optional.of(
                    Text.of(TextColors.AQUA, "[", TextColors.BLUE, "Staff Notice", TextColors.AQUA, "]",
                            TextColors.RESET, " ", original)
            );
        }
    };

    private Commands() {
        throw new UnsupportedOperationException("Utility class");
    }

    private static <T> T streamPlayerNotesSorted(Player p, Function<Stream<Note>, T> function) {
        return Note.findByPlayer(p,
                stream -> function.apply(stream.sorted()));
    }

    public static Text noteToText(Note note) {
        Tuple<TextColor, TextColor> colours = NOTE_TYPE_COLOURS.get(note.getType());
        TextColor bg = colours.getFirst(), fg = colours.getSecond();
        return Text.of(
                DATETIME_FORMAT.format(note.getDatetime()), " ", bg, "[", fg, note.getType(), bg, "] ",
                TextColors.RESET, note.getMessage(), " (left by ", note.getAuthor().getName(), ")"
        );
    }

    public static Text noteToText(Note note, long idx) {
        return Text.of("#", idx, " ").concat(noteToText(note));
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

    public static final CommandSpec REMOVE = CommandSpec.builder()
            .description(Text.of("Remove a note from a player"))
            .permission("frontex.note.remove")
            .arguments(
                    GenericArguments.user(Text.of("player")),
                    GenericArguments.integer(Text.of("note number"))
            )
            .executor((src, args) -> {
                User user = args.<User>getOne("player")
                        .orElseThrow(PLAYER_NOT_FOUND);
                int noteNumber = args.<Integer>getOne("note number")
                        .orElseThrow(() -> new CommandException(Text.of("Please provide a note nubmer")));

                Player playerDAO = Player.findByUUID(user.getUniqueId())
                        .orElseThrow(PLAYER_NOT_IN_DATABASE);
                Player authorDAO = Player.findByUUID(((User) src).getUniqueId())
                        .orElseThrow(AUTHOR_NOT_IN_DATABASE);

                Note toRemove = streamPlayerNotesSorted(playerDAO, stream -> stream.skip(noteNumber).findFirst())
                        .orElseThrow(() -> new CommandException(Text.of("Note ", noteNumber, " not found for " , user.getName())));

                if (!toRemove.delete())
                    throw new IllegalStateException("Failed to remove note from the database, please report this");

                STAFF_CHANNEL.send(Text.of(src.getName(), " deleted a note for ", user.getName()));
                src.sendMessage(Text.of(TextColors.AQUA, "Note deleted"));

                return CommandResult.success();
            })
            .build();

    public static final CommandSpec DEMOTE = CommandSpec.builder()
            .description(Text.of("Demote a note of a player"))
            .permission("frontex.note.demote")
            .arguments(
                    GenericArguments.user(Text.of("player")),
                    GenericArguments.integer(Text.of("note number"))
            )
            .executor((src, args) -> {
                User user = args.<User>getOne("player")
                        .orElseThrow(PLAYER_NOT_FOUND);
                int noteNumber = args.<Integer>getOne("note number")
                        .orElseThrow(() -> new CommandException(Text.of("Please provide a note nubmer")));

                Player playerDAO = Player.findByUUID(user.getUniqueId())
                        .orElseThrow(PLAYER_NOT_IN_DATABASE);
                Player authorDAO = Player.findByUUID(((User) src).getUniqueId())
                        .orElseThrow(AUTHOR_NOT_IN_DATABASE);

                Note note = streamPlayerNotesSorted(playerDAO, stream -> stream.skip(noteNumber).findFirst())
                        .orElseThrow(() -> new CommandException(Text.of("Note ", noteNumber, " not found for ", user.getName())));

                switch (note.getType()) {
                    case BAN:
                        note.setType(Note.Type.WARN);
                        break;
                    case WARN:
                        note.setType(Note.Type.INFO);
                        break;
                }

                if (!note.save()) {
                    throw new IllegalStateException("Failed to demote note in the database, please report this");
                }

                Note log = new Note(Instant.now(), playerDAO, authorDAO, Note.Type.LOG,
                                    "Demoted note level on note id " + note.getId());

                if (!log.save()) {
                    throw new IllegalStateException("Failed to create log note in the database, please report this");
                }

                STAFF_CHANNEL.send(Text.of(src.getName(), " demoted a note for ", user.getName()));
                src.sendMessage(Text.of(TextColors.AQUA, "Note demoted"));

                return CommandResult.success();
            })
            .build();

    public static final CommandSpec UNBAN = CommandSpec.builder()
            .description(Text.of("Unban a player"))
            .permission("frontex.unban")
            .arguments(GenericArguments.user(Text.of("player")))
            .executor((src, args) -> {
                User user = args.<User>getOne("player")
                        .orElseThrow(PLAYER_NOT_FOUND);

                Player playerDAO = Player.findByUUID(user.getUniqueId())
                        .orElseThrow(PLAYER_NOT_IN_DATABASE);
                Player authorDAO = Player.findByUUID(((User) src).getUniqueId())
                        .orElseThrow(AUTHOR_NOT_IN_DATABASE);

                Optional<Note> lastBan = Note.findByPlayer(playerDAO,
                         stream -> stream.filter(Note::isBan)
                            .peek((note) -> {
                                note.setType(Note.Type.WARN);
                                if (!note.save()) {
                                    throw new IllegalStateException("Unable to demote all ban notes, please report this");
                                }
                            }).max(Comparator.naturalOrder())); // get ban with highest ID, warning by now

                if (!lastBan.isPresent()) {
                    throw new CommandException(Text.of("It does not appear as if ", user.getName(), " is banned."));
                }

                Note log = new Note(Instant.now(), playerDAO, authorDAO, Note.Type.LOG, "Unbanned player");

                if (!log.save()) {
                    throw new IllegalStateException("Failed to create log note in the database, please report this");
                }

                // Fire unban event
                PardonUserEvent event = SpongeEventFactory.createPardonUserEvent(
                        Sponge.getCauseStackManager().getCurrentCause(), BanService.toBan(lastBan.get()), user);
                Sponge.getEventManager().post(event);

                Sponge.getServer().getBroadcastChannel().send(Text.of(TextColors.LIGHT_PURPLE, src.getName(), " has unbanned ", user.getName()));
                src.sendMessage(Text.of(TextColors.AQUA, "Done"));

                return CommandResult.success();
            })
            .build();

    public static final CommandSpec LIST = CommandSpec.builder()
            .description(Text.of("List your notes"))
            .arguments(
                    GenericArguments.requiringPermissionWeak(
                            GenericArguments.userOrSource(Text.of("player")),
                            "frontex.note.list"
                    )
            )
            .executor((src, args) -> {
                boolean isMod = src.hasPermission("frontex.note.list");
                // Needs to be orElseGet, otherwise the cast is executed unconditionally
                User user = args.<User>getOne("player").orElseGet(() -> (User) src);

                Player playerDAO = Player.findByUUID(user.getUniqueId())
                        .orElseThrow(PLAYER_NOT_FOUND);

                src.sendMessage(Text.of("- Notes logged for ", user.getName()));

                long noteCount = streamPlayerNotesSorted(playerDAO,
                        stream -> Streams.mapWithIndex(
                                        stream.filter((note) -> isMod || note.getType() == Note.Type.WARN),
                                        Commands::noteToText)
                                .peek(src::sendMessage)
                                .count());
                if (noteCount == 0) {
                    if (isMod) {
                        src.sendMessage(Text.of(TextColors.DARK_RED, "There are no notes for ", user.getName()));
                    } else {
                        src.sendMessage(Text.of(TextColors.DARK_RED, "You have no warnings"));
                    }
                }
                return CommandResult.success();
            })
            .build();

    public static final CommandSpec INFO = CommandSpec.builder()
            .description(Text.of("Add an info note for the player"))
            .permission("frontex.note.info")
            .arguments(
                    GenericArguments.user(Text.of("player")),
                    GenericArguments.remainingJoinedStrings(Text.of("message"))
            )
            .executor(new AddNoteExecutor(Note.Type.INFO))
            .build();

    public static final CommandSpec WARN = CommandSpec.builder()
            .description(Text.of("Add a warning note for the player"))
            .permission("frontex.note.warn")
            .arguments(
                    GenericArguments.user(Text.of("player")),
                    GenericArguments.remainingJoinedStrings(Text.of("message"))
            )
            .executor(new AddNoteExecutor(Note.Type.WARN))
            .build();


    public static final CommandSpec BAN = CommandSpec.builder()
            .description(Text.of("Ban a player"))
            .permission("frontex.ban")
            .arguments(
                    GenericArguments.user(Text.of("player")),
                    GenericArguments.remainingJoinedStrings(Text.of("message"))
            )
            .executor((src, args) -> {
                if (!(src instanceof org.spongepowered.api.entity.living.player.Player)) {
                    throw new CommandException(Text.of("This command can only be executed by a player"));
                }

                User user = args.<User>getOne("player")
                        .orElseThrow(PLAYER_NOT_FOUND);

                Player playerDAO = Player.findByUUID(user.getUniqueId())
                        .orElseThrow(PLAYER_NOT_IN_DATABASE);
                Player authorDAO = Player.findByUUID(((User) src).getUniqueId())
                        .orElseThrow(AUTHOR_NOT_IN_DATABASE);

                // Use orElseGet to avoid 'expensive' computation
                String message = args.<String>getOne("message")
                        .orElseGet(() -> "Banned by " + authorDAO.getName() + " at "
                                + DATETIME_FORMAT.format(Instant.now()));

                boolean alreadyBanned = Note.findByPlayer(playerDAO, stream -> stream.anyMatch(Note::isBan));
                Note note = new Note(Instant.now(), playerDAO, authorDAO, Note.Type.BAN, message);
                note.save();

                STAFF_CHANNEL.send(Text.of(src.getName(), " added a note for ", user.getName()));
                STAFF_CHANNEL.send(noteToText(note));

                if (alreadyBanned) {
                    // No need to fire events, kick players or announce bans
                    return CommandResult.empty();
                }

                // Fire ban event
                Cause cause = Sponge.getCauseStackManager().getCurrentCause();
                Ban.Profile ban = (Ban.Profile) Ban.builder()
                        .type(BanTypes.PROFILE)
                        .profile(user.getProfile())
                        .reason(Text.of(message))
                        .startDate(note.getDatetime())
                        .source(src)
                        .build();

                // Fire BanUserEvent.TargetPlayer if we have a player
                // Specify return type of map lambda to be able to use less specific orElse
                BanUserEvent event = user.getPlayer()
                        .<BanUserEvent>map((player) ->
                                SpongeEventFactory.createBanUserEventTargetPlayer(cause, ban, player, user))
                        .orElse(SpongeEventFactory.createBanUserEvent(cause, ban, user));
                Sponge.getEventManager().post(event);

                user.getPlayer().ifPresent((player) ->
                        player.kick(Text.of("Permanently banned: ", message)));

                long banCount = Note.countDistinctByType(Note.Type.BAN);
                Sponge.getServer().getBroadcastChannel().send(
                        Text.of(
                        TextActions.showText(Text.of(TextColors.GREEN, user.getName()+" banned for: ", TextColors.WHITE, message)),
						TextColors.LIGHT_PURPLE, user.getName(), " was hit by the banhammer! (", banCount, " bans)"
                        )
		);

                return CommandResult.success();
            })
            .build();
    
    public static final CommandSpec NOTE = CommandSpec.builder()
            .child(REMOVE, "remove", "rem", "delete", "del")
            .child(DEMOTE, "demote")
            .child(LIST, "list")
            .child(INFO, "info")
            .child(WARN, "warn")
            .child(BAN, "ban") // For those times you forgot ban is its own command
            .build();
    
    public static final CommandSpec REASON = CommandSpec.builder()
            .description(Text.of("Check why a player is banned"))
            .permission("frontex.reason")
            .arguments(GenericArguments.user(Text.of("player")))
            .executor((src, args) -> {
                User user = args.<User>getOne("player")
                        .orElseThrow(PLAYER_NOT_FOUND);
                Player playerDAO = Player.findByUUID(user.getUniqueId())
                        .orElseThrow(PLAYER_NOT_IN_DATABASE);

                Note note = Note.findByPlayer(playerDAO, stream ->
                        stream.filter(Note::isBan)
                                .max(Comparator.naturalOrder()) // Latest ban note
                ).orElseThrow(() -> new CommandException(Text.of(playerDAO.getName(), " is not banned.")));

                src.sendMessage(
                        Text.of("Ban reason for ", user.getName(), ": ", TextColors.LIGHT_PURPLE, note.getMessage())
                );

                return CommandResult.success();
            })
            .build();

    @RequiredArgsConstructor
    public static class AddNoteExecutor implements CommandExecutor {
        @NonNull
        private final Note.Type noteType;

        @Override
        public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
            if (!(src instanceof org.spongepowered.api.entity.living.player.Player)) {
                throw new IllegalArgumentException("This command can only be executed by a player");
            }

            User user = args.<User>getOne("player")
                    .orElseThrow(PLAYER_NOT_FOUND);
            String message = args.<String>getOne("message")
                    .orElseThrow(() -> new CommandException(Text.of("Please provide a message")));

            Player playerDAO = Player.findByUUID(user.getUniqueId())
                    .orElseThrow(PLAYER_NOT_IN_DATABASE);
            Player authorDAO = Player.findByUUID(((User) src).getUniqueId())
                    .orElseThrow(AUTHOR_NOT_IN_DATABASE);

            Note note = new Note(Instant.now(), playerDAO, authorDAO, noteType, message);
            note.save();

            STAFF_CHANNEL.send(Text.of(src.getName(), " added a note for ", user.getName()));
            STAFF_CHANNEL.send(noteToText(note));

            if (noteType == Note.Type.WARN) {
                user.getPlayer().ifPresent((player) -> {
                    player.sendMessages(
                            Text.of(TextColors.RED, src.getName(), " has logged a warning for you into the system:"),
                            Text.of(TextColors.RED, "\"", TextColors.WHITE, message, TextColors.RED, "\"")
                    );
                });
            }

            return CommandResult.success();
        }
    }
}
